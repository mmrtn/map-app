# MapApp

Rakendus on kirjutatud Angular 5.2 raamiskitule TypeScriptga ja kasutab  Angular CLI-d.

## DEV KESKKONNA SEADISTAMINE

Projekti  seadistamiseks on vajalik  vähemalt Node 6.9.0 ja npm 3 või uuem

Kui Angular CLI pole veel paigaldatud, siis tuleb seda teha:

`npm install -g @angular/cli`

Kui projekt on Bitbucketist allalaetud, siis teha:

`npm install`

### Leaflet ja ngx-leaflet

Projekt kasutab kaartimootorit Leaflet ja Angularile kirjutatut teeki ngx-leaflet.
Ennem käivitamist installeerida leaflet ja sellele vastav teek:

`npm install leaflet`

`npm install @asymmetrik/ngx-leaflet`

`npm install --save-dev @types/leaflet`

### Angular Material

Projektis on kasutatud Angular Materiali komponente
Angular Materiali installeerimiseks tuleb teha:

`npm install --save @angular/material @angular/cdk`

`npm install --save @angular/animations`

## Projekti käivitamine

Projekti  käivitamiseks tuleb projekti juurkaustas kasutada käsku:
`ng serve` mis käivitab lokaalse arendusserveri http://localhost:4200

Vajadusel saab projekti käivitada mõne teise pordi pealt: `ng serve --port xxxx`