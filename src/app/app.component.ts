import {Component, NgZone} from '@angular/core';
import {ClosestMarker, Coords, MapObject} from '../models/models';
import * as L from 'leaflet';
import {latLng, LayerGroup, Map, Marker, tileLayer} from 'leaflet';
import {StorageHelper} from '../shared/storage.helper';
import {Subject} from 'rxjs/Subject';
import {dropIn} from '../shared/animations';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    animations: [
        dropIn,
    ]
})
export class AppComponent {

    static obs = new Subject();
    mapObject: MapObject;
    mapObjects: MapObject[] = [];
    toggleForm = false;

    markerGroup: LayerGroup;
    map: Map;

    activeIconId: number;

    options = {
        layers: [
            tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18, attribution: '...'})
        ],
        zoom: 14,
        center: latLng(58.38293090324065, -333.26429843902594)
    };

    startMarking = false;

    constructor(private zone: NgZone) {
        this.resetMapObject();
        const mapObjects = StorageHelper.loadJsonObject('mapObjects');
        if (mapObjects) {
            this.mapObjects = <MapObject[]>mapObjects;
            this.updateTableData(this.mapObjects);
        }
    }

    markOnMap() {
        this.toggleForm = true;
        if (!this.startMarking) {
            this.startMarking = true;
            if (this.mapObject.id) {
                this.removeMarker(this.mapObject.id);
            }
        } else {
            this.startMarking = false;
        }
    }

    onMapReady(map: Map) {
        this.map = map;
        this.markerGroup = L.layerGroup().addTo(this.map);
        const that = this;

        if (this.mapObjects.length > 0) {
            for (const obj of this.mapObjects) {
                this.addToMap(obj);
            }
            this.updateClosestMarkers();
        }
        map.on('click', (e) => {
            const popLocation = e['latlng'];
            if (popLocation && that.startMarking) {
                that.mapObject.coordinates = <Coords>popLocation;
                const id = that.addToMap(that.mapObject, 'yellow-icon');
                that.startMarking = false;
                that.zone.run(() => {
                });
            }
        });
    }

    addToMap(mapObject: MapObject, iconClass = 'green-icon'): number {
        const that = this;
        const marker = L.marker(mapObject.coordinates, {

            icon: L.divIcon({className: iconClass}), // default class : leaflet-div-icon
            draggable: true,
            title: mapObject.name
        })
            .bindPopup(`<h4>${mapObject.name}</h4><p>${mapObject.description}</p>`)
            .addTo(this.markerGroup);

        mapObject.id = this.markerGroup.getLayerId(marker);

        marker.on('click', (e) => {
            that.activeIconId = mapObject.id;
            that.updateTableData([...that.mapObjects]);
        });
        marker.on('dragend', (e) => {
            setTimeout(() => {
                that.updateObjectCoordiantes(marker);
            }, 10);
        });

        return mapObject.id;
    }

    updateObjectCoordiantes(marker) {
        const id = this.markerGroup.getLayerId(marker);
        if (this.mapObject.id === id) {
            this.mapObject.coordinates = marker.getLatLng();
            this.zone.run(() => {
            });
        } else {
            const mapObject = this.mapObjects.find((obj) => obj.id === id);
            mapObject.coordinates = marker.getLatLng();
            this.updateClosestMarkers();
            this.updateTableData(this.mapObjects);
            StorageHelper.saveJsonObject('mapObjects', this.mapObjects);
        }
    }

    cancel() {
        this.startMarking = false;
        if (this.mapObject.id) {
            this.removeMarker(this.mapObject.id);
        }
        this.resetMapObject();
    }

    canSave(): boolean {
        return (this.mapObject.coordinates && !!this.mapObject.name &&
            !(this.isNameExistsAlready()));
    }

    unsavedNewMarker(): boolean {
        if (this.mapObject.coordinates) {
            return true;
        }
        return false;
    }

    isNameExistsAlready(): boolean {
        return !!(this.mapObjects.find((obj) => obj.name === this.mapObject.name));
    }

    save() {
        if (this.canSave()) {
            this.mapObjects.push(this.mapObject);
            this.changeMarkerIconClass(this.mapObject.id);
            const marker = this.markerGroup.getLayer(this.mapObject.id)
                .bindPopup(`<h4>${this.mapObject.name}</h4><p>${this.mapObject.description}</p>`);

            this.resetMapObject();
            this.updateClosestMarkers();
            this.updateTableData(this.mapObjects);
            StorageHelper.saveJsonObject('mapObjects', this.mapObjects);
        }
    }

    changeMarkerIconClass(id: number, className = 'green-icon') {
        const marker = this.markerGroup.getLayer(id);

        this.zone.run(() => {
            this.markerGroup.removeLayer(marker);
            if (marker) {
                marker['options'].icon.options.className = className;
                marker.addTo(this.markerGroup);
            }
        });
    }

    isMarker(id): boolean {
        const allMarkers = Object.keys(this.markerGroup['_layers']);
        return allMarkers.indexOf(id + '') > -1;
    }

    removeMarker(id: number) {
        if (this.isMarker(id)) {
            this.markerGroup.removeLayer(id);
            this.mapObjects = this.mapObjects.filter((obj) => obj.id !== id);
            this.updateClosestMarkers();
            this.updateTableData(this.mapObjects);
            StorageHelper.saveJsonObject('mapObjects', this.mapObjects);
        }
    }

    fireClickOnMarker(id: number) {
        const marker = this.markerGroup.getLayer(id);
        marker.fireEvent('click');
    }

    resetMapObject() {
        this.mapObject = {
            coordinates: null,
            name: '',
            description: ''
        };
    }

    updateTableData(mapObjects: MapObject[]) {
        setTimeout(() => {
            AppComponent.obs.next(mapObjects);
        }, 100);
    }

    getCoordinates(): string {
        if (this.mapObject.coordinates) {
            return this.mapObject.coordinates.lat + ',' + this.mapObject.coordinates.lng;
        }
        return '';
    }

    findClosest(id): ClosestMarker {
        const marker = <Marker>this.markerGroup.getLayer(id);
        const coords = marker.getLatLng();
        let minDist;
        let name;
        const markersKeys = Object.keys(this.markerGroup['_layers']);
        if (markersKeys.length < 2) {
            return null;
        }
        for (const obj of this.mapObjects) {
            if (obj.id && obj.id !== id) {
                const thisMarker = <Marker>this.markerGroup.getLayer(obj.id);
                const thisCoords = thisMarker.getLatLng();
                const distance = thisCoords.distanceTo(coords);
                if (!minDist || minDist > distance) {
                    minDist = distance;
                    name = thisMarker.options.title;
                    if (!name) {
                        name = obj.name;
                    }
                }
            }
        }
        if (minDist < 500) {
            this.changeMarkerIconClass(id, 'red-icon');
        } else {
            this.changeMarkerIconClass(id, 'green-icon');
        }

        return {name: name, distance: minDist};

    }

    updateClosestMarkers() {
        if (this.mapObjects.length > 1) {
            for (const obj of this.mapObjects) {
                obj.closestMarker = this.findClosest(obj.id);
            }
        } else {
            if (this.mapObjects.length) {
                this.mapObjects[0].closestMarker = null;
            }
        }
    }

}
