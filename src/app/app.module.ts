import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ServiceWorkerModule} from '@angular/service-worker';
import {MaterialModule} from './material/material.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';

import {environment} from '../environments/environment';
// import {MatButtonModule, MatInputModule} from '@angular/material';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DatatableComponent } from './datatable/datatable.component';


@NgModule({
    declarations: [
        AppComponent,
        DatatableComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        MaterialModule,

        LeafletModule.forRoot(),
        LeafletModule,

        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production})
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
