import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnInit, Output} from '@angular/core';
import {ClosestMarker, Coords, MapObject} from '../../models/models';
import {AppComponent} from '../app.component';
import {MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-datatable',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit, AfterViewInit {

    displayedColumns = ['delete', 'Nr', 'name', 'description', 'coordinates', 'closestObject'];
    dataSource;
    noClickOnRow = false;

    @Input() activeRow: number;

    mapObjects: MapObject[] = [];
    @Output() rowRemoved = new EventEmitter<number>();
    @Output() clickOnRow = new EventEmitter<number>();

    constructor(private zone: NgZone) {
    }

    ngAfterViewInit() {
    }

    ngOnInit() {

        if (this.mapObjects) {
            this.dataSource = new MatTableDataSource(this.mapObjects);
        }

        AppComponent.obs.subscribe(data => {

            // console.log(' AppComponent.obs.subscribe(data =>'+ JSON.stringify(data));

            this.mapObjects = <MapObject[]>data;
            this.zone.run(() => {
                this.dataSource = new MatTableDataSource(this.mapObjects);
            });
        });

    }

    mouseOverRemoveButton(value) {
        this.noClickOnRow = value;
    }

    rowClicked(row) {
        if (!this.noClickOnRow) {
            this.clickOnRow.emit(row.id);
            this.activeRow = row.id;
        }
    }

    remove(row) {
        this.rowRemoved.emit(row.id);
    }

    getCoordinates(coords: Coords): string {
        return `${coords.lat.toFixed(5)},${coords.lng.toFixed(5)}`;
    }

    getClosestObject(closestMarker: ClosestMarker): string {
        if (!closestMarker || !closestMarker.distance) {
            return '';
        }
        return `${closestMarker.name} - ${(closestMarker.distance < 1000) ?
            Math.round(closestMarker.distance) + 'm' : (closestMarker.distance / 1000).toFixed(1) + 'km'}`;
    }

}
