import {LatLng} from 'leaflet';

export interface Coords {
    lat: number;
    lng: number;
    alt?: number;
}

export interface ClosestMarker {
    name: string;
    distance: number;
}


export interface MapObject {
    id?: number;
    name: string;
    description: string;
    coordinates: Coords;
    closestMarker?: ClosestMarker;
}
