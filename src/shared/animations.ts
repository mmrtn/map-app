import {animate, state, style, transition, trigger} from '@angular/animations';


export const dropIn = trigger('dropIn', [
    state('open', style({
        transform: 'translateY(0%)'
    })),
    state('close', style({
        transform: 'translateY(-100%)',
        opacity: 0.5
    })),

    transition('void <=> *', [
        style({transform: 'translateY(-100%)'}),
        animate(500)
    ]),


    transition('open => close', animate('600ms ease-in')),
    transition('close => open', animate('600ms ease-out'))
]);

