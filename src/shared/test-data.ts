import {MapObject} from '../models/models';

export const testData: MapObject[] = [{
    'coordinates': {'lat': 58.379758553327704, 'lng': -333.28742980957037},
    'name': 'Home',
    'description': 'Sõpruse pst 8-35',
    'id': 68
}, {
    'coordinates': {'lat': 58.37917354984813, 'lng': -333.26691627502447},
    'name': 'Work',
    'description': 'Töökoht kusagil kesklinnas, Pläsku ümbruses',
    'id': 71
}, {
    'coordinates': {'lat': 58.35022626900405, 'lng': -333.23730468750006},
    'name': 'Mingi koht',
    'description': 'linna äärne pleiss....',
    'id': 74
}, {
    'coordinates': {'lat': 58.394515431160926, 'lng': -333.22494506835943},
    'name': 'My Old School',
    'description': 'Tartu Raaturse Gümnaasium',
    'id': 77
}, {
    'coordinates': {'lat': 58.401711667608, 'lng': -333.28399658203125},
    'name': 'Toomemägi',
    'description': 'Cool Toomekas',
    'id': 80
}, {
    'coordinates': {'lat': 58.40099211006947, 'lng': -333.3705139160156},
    'name': 'Kaarsild',
    'description': 'Sild kust saab üle kõndida',
    'id': 83
}, {
    'coordinates': {'lat': 58.37507825384993, 'lng': -333.31832885742193},
    'name': 'Selver',
    'description': 'Jaamamõisa Selver',
    'id': 86
}];
